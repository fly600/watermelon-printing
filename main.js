import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

Vue.prototype.checkLogin = function(){
	const isLogin = uni.getStorageSync('isLogin')
	let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
	let curRoute = routes[routes.length - 1].route //获取当前页面路由   //   pages/webview/webview
	if(!isLogin){ // 本地没有token表示未登录
		console.log('未登录返回到登录页')
		uni.reLaunch({
			url:"/pages/my/uCenter/uCenter?path="+curRoute,
			fail: (err) => {
				console.log(err)
			}
		});
		return false
	}
}
import $http from '@/uni_modules/zhouWei-request/js_sdk/requestConfig';
Vue.prototype.$http = $http;
const app = new Vue({
    ...App
})
app.$mount()
