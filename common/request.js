let baseUrl = "http://xgprint.com/xgprint/api."
// 用户登录 Token 数据键名称
let	userTokenKeyName  = 'loginToken'
// token 有效期, 单位 秒 ，要与后端保持一致
let expiredTime =3600
// post 方式 header[content-type] 默认值
let	postHeaderDefault = 'application/x-www-form-urlencoded'
module.exports = {

	request:function(option){
		return new Promise((resolve,reject)=>{
				uni.request({
					url: baseUrl + option.url,
					method:option.method || 'GET',
					data:option.data || {},
					header:{
						...option.header
					},
					success: (res) => {
						resolve(res)
					},
					fail: (err) => {
						reject(err)
					}
				})
			})
	},
	//检查用户token
	checkLogin:function(){
		let loginToken = uni.getStorageSync(userTokenKeyName)
		if(!loginToken || loginToken == ''){
			loginToken = '';
			uni.showToast({title:"请登录", icon:"none", mask:true});
			setTimeout(()=>{this.gotoLogin();}, 1500);
			return false;
		}
		return loginToken;
	},
		// 跳转到登录页面
	gotoLogin : function (path, opentype) {
		if(!path){path = '../login/login';}
		if(!opentype){opentype = 'redirect';}
		switch(opentype){
			case  'redirect' : 
			uni.redirectTo({url:path});
			break;
			case  'navigate' : 
			uni.navigateTo({url:path});
			break;
			case  'switchTab' : 
			uni.switchTab({url:path});
			break;
		}
	},
}

